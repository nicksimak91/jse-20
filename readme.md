# TASK MANAGER

## DEVELOPER INFO

**NAME:** Nikita Simakov

**E-MAIL:** nasimakov@t1-consulting.ru

## SYSTEM INFO

**OS**: Windows 11

**JDK**: Java 1.8

**RAM**: 16GB

**CPU**: Rayzen 7

## BUILD PROJECT

```
mvn clean install
```

## RUN PROJECT
```
cd ./target
java -jar ./task-manager.jar
```