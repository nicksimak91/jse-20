package ru.t1.simakov.tm.repository;

import ru.t1.simakov.tm.api.repository.IProjectRepository;
import ru.t1.simakov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return add(project);
    }

}
