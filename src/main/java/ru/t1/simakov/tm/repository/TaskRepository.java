package ru.t1.simakov.tm.repository;

import ru.t1.simakov.tm.api.repository.ITaskRepository;
import ru.t1.simakov.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    private Predicate<Task> filterByProjectId(final String projectId) {
        return m -> projectId.equals(m.getProjectId());
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .filter(filterByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

}
