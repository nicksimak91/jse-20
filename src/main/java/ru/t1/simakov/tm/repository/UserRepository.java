package ru.t1.simakov.tm.repository;

import ru.t1.simakov.tm.api.repository.IUserRepository;
import ru.t1.simakov.tm.api.service.IPropertyService;
import ru.t1.simakov.tm.enumerated.Role;
import ru.t1.simakov.tm.model.User;
import ru.t1.simakov.tm.service.PropertyService;
import ru.t1.simakov.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(String id) {
        for (User user: models.values()) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (User user: models.values()) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (User user: models.values()) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        for (final User user: models.values()) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for (final User user: models.values()) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        final IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return add(user);
    }

}
