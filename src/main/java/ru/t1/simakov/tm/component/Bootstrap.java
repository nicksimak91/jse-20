package ru.t1.simakov.tm.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.simakov.tm.api.repository.ICommandRepository;
import ru.t1.simakov.tm.api.repository.IProjectRepository;
import ru.t1.simakov.tm.api.repository.ITaskRepository;
import ru.t1.simakov.tm.api.repository.IUserRepository;
import ru.t1.simakov.tm.api.service.*;
import ru.t1.simakov.tm.command.AbstractCommand;
import ru.t1.simakov.tm.command.project.*;
import ru.t1.simakov.tm.command.system.*;
import ru.t1.simakov.tm.command.task.*;
import ru.t1.simakov.tm.command.user.*;
import ru.t1.simakov.tm.enumerated.Role;
import ru.t1.simakov.tm.enumerated.Status;
import ru.t1.simakov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.simakov.tm.exception.system.CommandNotSupportedException;
import ru.t1.simakov.tm.model.Project;
import ru.t1.simakov.tm.model.Task;
import ru.t1.simakov.tm.repository.CommandRepository;
import ru.t1.simakov.tm.repository.ProjectRepository;
import ru.t1.simakov.tm.repository.TaskRepository;
import ru.t1.simakov.tm.repository.UserRepository;
import ru.t1.simakov.tm.service.*;
import ru.t1.simakov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    private final ICommandRepository commandsRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandsService = new CommandService(commandsRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IPropertyService propertyService = new PropertyService();

    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePassword());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandsService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    public void run(final String... args) {
        if (parseArguments(args)) return;
        parseCommands();
    }

    public void parseCommands() {
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }

        }
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandsService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandsService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private boolean parseArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandsService.add(command);
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("BETA PROJECT", Status.COMPLETED));

        taskService.add(new Task("MEGA TASK", Status.COMPLETED));
        taskService.add(new Task("BETA TASK", Status.IN_PROGRESS));
        taskService.add(new Task("ALFA TASK", Status.NOT_STARTED));
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
            }
        });
    }

    public IPropertyService getPropertyService() {
        return propertyService;
    }

}
