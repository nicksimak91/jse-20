package ru.t1.simakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simakov.tm.api.service.IAuthService;
import ru.t1.simakov.tm.api.service.IPropertyService;
import ru.t1.simakov.tm.api.service.IUserService;
import ru.t1.simakov.tm.enumerated.Role;
import ru.t1.simakov.tm.exception.field.LoginEmptyException;
import ru.t1.simakov.tm.exception.field.PasswordEmptyException;
import ru.t1.simakov.tm.exception.system.AccessDeniedException;
import ru.t1.simakov.tm.exception.system.AuthenticationException;
import ru.t1.simakov.tm.exception.system.PermissionException;
import ru.t1.simakov.tm.model.User;
import ru.t1.simakov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        final boolean locked = user.isLocked() == null || user.isLocked();
        if (locked) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();

    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
