package ru.t1.simakov.tm.service;

import ru.t1.simakov.tm.api.repository.IProjectRepository;
import ru.t1.simakov.tm.api.repository.ITaskRepository;
import ru.t1.simakov.tm.api.service.IProjectTaskService;
import ru.t1.simakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.simakov.tm.exception.entity.TaskNotFoundException;
import ru.t1.simakov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.simakov.tm.exception.field.TaskIdEmptyException;
import ru.t1.simakov.tm.exception.field.UserIdEmptyException;
import ru.t1.simakov.tm.model.Project;
import ru.t1.simakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            IProjectRepository projectRepository,
            ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.forEach(t -> taskRepository.removeById(userId, t.getId()));
        projectRepository.removeById(userId, projectId);
    }

    public void removeProjects(final String userId) {
        final List<Project> projects = projectRepository.findAll(userId);
        for (Project project: projects) {
            if (project == null) continue;
            removeProjectById(userId, project.getId());
        }
    }

}
