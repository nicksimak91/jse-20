package ru.t1.simakov.tm.service;

import ru.t1.simakov.tm.api.repository.ICommandRepository;
import ru.t1.simakov.tm.api.service.ICommandService;
import ru.t1.simakov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    public final ICommandRepository commandsRepository;

    public CommandService(final ICommandRepository commandsRepository) {
        this.commandsRepository = commandsRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandsRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandsRepository.getCommandByArgument(argument);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandsRepository.getCommandByName(name);
    }

    public Collection<AbstractCommand> getTerminalCommands() {
        return commandsRepository.getTerminalCommands();
    }

}
