package ru.t1.simakov.tm.service;

import ru.t1.simakov.tm.api.repository.IRepository;
import ru.t1.simakov.tm.api.service.IService;
import ru.t1.simakov.tm.enumerated.Sort;
import ru.t1.simakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.simakov.tm.exception.field.IdEmptyException;
import ru.t1.simakov.tm.exception.field.IndexIncorrectException;
import ru.t1.simakov.tm.model.AbstractModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        if (repository == null) return Collections.emptyList();
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public M add(M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public M findOneById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ProjectNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new ProjectNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
