package ru.t1.simakov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.simakov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application_version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author_email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "author_name";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password_iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "65452";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password_secret";

    @NotNull
    private static final String EMPTY_VALUE = "--//--";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        try {
            properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getStringValue(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return getStringValue(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return getStringValue(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}


