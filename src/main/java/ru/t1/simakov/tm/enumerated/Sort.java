package ru.t1.simakov.tm.enumerated;

import com.sun.org.apache.bcel.internal.generic.ATHROW;
import ru.t1.simakov.tm.comparator.CreatedComparator;
import ru.t1.simakov.tm.comparator.NameComparator;
import ru.t1.simakov.tm.comparator.StatusComparator;
import ru.t1.simakov.tm.exception.sorting.SortTypeEmptyException;
import ru.t1.simakov.tm.exception.sorting.SortTypeNotFoundException;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),

    BY_STATUS("Sort by status", StatusComparator.INSTANCE),

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator<?> comparator;

    Sort(final String displayName, final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) throw new SortTypeEmptyException();
        for (final Sort sort: values()) {
            if (sort.name().equals(value)) return sort;
        }
        throw new SortTypeNotFoundException();
    }

    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

}
