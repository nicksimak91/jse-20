package ru.t1.simakov.tm.api.service;

import ru.t1.simakov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    void removeProjects(String userId);

}
