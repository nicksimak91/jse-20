package ru.t1.simakov.tm.api.component;

public interface IBootstrap {
    void run(String... args);

}
