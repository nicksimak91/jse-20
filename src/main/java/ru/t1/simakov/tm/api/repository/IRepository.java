package ru.t1.simakov.tm.api.repository;

import ru.t1.simakov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void deleteAll();

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M add(M model);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    boolean existsById(String id);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    Integer getSize();

}
