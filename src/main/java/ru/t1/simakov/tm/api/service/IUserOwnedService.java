package ru.t1.simakov.tm.api.service;

import ru.t1.simakov.tm.enumerated.Sort;
import ru.t1.simakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M>{

    void clear(String userId);

    boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    Integer getSize(String userId);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M add(String userId, M model);

}
