package ru.t1.simakov.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IAuthService getAuthService();

    IUserService getUserService();

    IPropertyService getPropertyService();

}
