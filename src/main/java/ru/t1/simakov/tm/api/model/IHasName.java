package ru.t1.simakov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
