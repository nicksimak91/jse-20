package ru.t1.simakov.tm.api.model;

import ru.t1.simakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
