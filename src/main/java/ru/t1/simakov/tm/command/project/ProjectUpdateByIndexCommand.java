package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Update task by index.";
    }

    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}
