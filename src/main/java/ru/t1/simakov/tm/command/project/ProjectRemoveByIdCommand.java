package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.model.Project;
import ru.t1.simakov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().removeById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
