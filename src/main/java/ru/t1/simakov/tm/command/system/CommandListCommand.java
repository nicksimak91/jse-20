package ru.t1.simakov.tm.command.system;

import ru.t1.simakov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-c";
    }

    @Override
    public String getDescription() {
        return "Show application commands.";
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
