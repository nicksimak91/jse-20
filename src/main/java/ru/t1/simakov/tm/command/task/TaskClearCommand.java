package ru.t1.simakov.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = getUserId();
        getTaskService().clear(userId);
        System.out.println("[OK]");
    }

}
