package ru.t1.simakov.tm.command.task;

import ru.t1.simakov.tm.enumerated.Status;
import ru.t1.simakov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Task change status by index.";
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

}
