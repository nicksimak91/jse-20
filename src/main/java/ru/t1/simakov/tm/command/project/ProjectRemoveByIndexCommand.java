package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().removeByIndex(userId, index);
    }

}
