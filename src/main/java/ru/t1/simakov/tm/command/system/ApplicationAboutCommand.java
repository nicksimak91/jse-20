package ru.t1.simakov.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: " + getPropertyService().getAuthorName());
        System.out.println("EMAIL: " +  getPropertyService().getAuthorEmail());
    }

}
