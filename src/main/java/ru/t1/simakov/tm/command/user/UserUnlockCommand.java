package ru.t1.simakov.tm.command.user;

import ru.t1.simakov.tm.enumerated.Role;
import ru.t1.simakov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "user unlock";
    }

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
