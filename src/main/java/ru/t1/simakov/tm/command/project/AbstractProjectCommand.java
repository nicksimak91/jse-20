package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.api.service.IProjectService;
import ru.t1.simakov.tm.api.service.IProjectTaskService;
import ru.t1.simakov.tm.command.AbstractCommand;
import ru.t1.simakov.tm.enumerated.Role;
import ru.t1.simakov.tm.enumerated.Status;
import ru.t1.simakov.tm.model.Project;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    public String getArgument() {
        return null;
    }

    protected static void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project: projects) {
            if (project == null) continue;
            final String name = project.getName();
            final String description = project.getDescription();
            final String status = Status.toName(project.getStatus());
            System.out.printf("%s. %s : %s %s \n", index, name, description, status);
            index++;
        }
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
