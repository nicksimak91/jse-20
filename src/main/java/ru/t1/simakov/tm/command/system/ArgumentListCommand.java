package ru.t1.simakov.tm.command.system;

import ru.t1.simakov.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show application arguments.";
    }

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
}
