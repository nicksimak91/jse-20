package ru.t1.simakov.tm.command.system;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show application version.";
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
