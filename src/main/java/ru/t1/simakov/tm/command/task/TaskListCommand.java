package ru.t1.simakov.tm.command.task;

import ru.t1.simakov.tm.enumerated.Sort;
import ru.t1.simakov.tm.model.Task;
import ru.t1.simakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Show task list.";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
        System.out.println("[OK]");
    }

}
