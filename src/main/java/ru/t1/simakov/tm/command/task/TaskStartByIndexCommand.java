package ru.t1.simakov.tm.command.task;

import ru.t1.simakov.tm.enumerated.Status;
import ru.t1.simakov.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
