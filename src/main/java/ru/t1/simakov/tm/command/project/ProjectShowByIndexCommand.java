package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.model.Project;
import ru.t1.simakov.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Display task by index.";
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}
