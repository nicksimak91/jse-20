package ru.t1.simakov.tm.command.task;

import ru.t1.simakov.tm.model.Task;
import ru.t1.simakov.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Display task by id.";
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}
