package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

}
