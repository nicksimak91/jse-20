package ru.t1.simakov.tm.command.project;

import ru.t1.simakov.tm.enumerated.Status;
import ru.t1.simakov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Project change status by id.";
    }

    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, status);
    }

}
