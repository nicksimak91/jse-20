package ru.t1.simakov.tm.command.task;

import ru.t1.simakov.tm.enumerated.Status;
import ru.t1.simakov.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Complete task by index.";
    }

    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
    }

}
