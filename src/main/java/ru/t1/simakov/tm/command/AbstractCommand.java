package ru.t1.simakov.tm.command;

import ru.t1.simakov.tm.api.model.ICommand;
import ru.t1.simakov.tm.api.service.IAuthService;
import ru.t1.simakov.tm.api.service.IPropertyService;
import ru.t1.simakov.tm.api.service.IServiceLocator;
import ru.t1.simakov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ", " + description;
        return result;
    }

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }


}
