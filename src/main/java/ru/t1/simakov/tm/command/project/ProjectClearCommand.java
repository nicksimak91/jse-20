package ru.t1.simakov.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = getUserId();
        getProjectTaskService().removeProjects(userId);
        System.out.println("[OK]");
    }

}
