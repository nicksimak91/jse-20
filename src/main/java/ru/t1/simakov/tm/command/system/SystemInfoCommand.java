package ru.t1.simakov.tm.command.system;

import ru.t1.simakov.tm.command.AbstractCommand;

import java.util.Collection;

public class SystemInfoCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Show application help.";
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

}
