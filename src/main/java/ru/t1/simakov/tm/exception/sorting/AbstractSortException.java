package ru.t1.simakov.tm.exception.sorting;

import ru.t1.simakov.tm.exception.AbstractException;

public class AbstractSortException extends AbstractException {

    public AbstractSortException() {
    }

    public AbstractSortException(String message) {
        super(message);
    }

    public AbstractSortException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractSortException(Throwable cause) {
        super(cause);
    }

    public AbstractSortException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
