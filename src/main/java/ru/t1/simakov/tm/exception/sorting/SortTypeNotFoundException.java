package ru.t1.simakov.tm.exception.sorting;

public class SortTypeNotFoundException extends AbstractSortException {

    public SortTypeNotFoundException() {
        super("Error! Sort type not found...");
    }

}
