package ru.t1.simakov.tm.exception.field;

public class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! userId is empty...");
    }

}
