package ru.t1.simakov.tm.exception.field;

public class DescriptionEmptyException extends AbstractFieldException{

    public DescriptionEmptyException() {
        super("Error! Description is empty...");
    }

}
