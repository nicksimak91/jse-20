package ru.t1.simakov.tm.exception.system;

public class AuthenticationException extends AbstractSystemException {

    public AuthenticationException() {
        super("Error! Incorrect login or password. Please try again.");
    }

}
