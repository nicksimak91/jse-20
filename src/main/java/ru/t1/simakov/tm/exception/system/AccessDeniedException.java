package ru.t1.simakov.tm.exception.system;

import ru.t1.simakov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access is denied.");
    }

}
