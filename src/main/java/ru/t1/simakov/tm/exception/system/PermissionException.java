package ru.t1.simakov.tm.exception.system;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
