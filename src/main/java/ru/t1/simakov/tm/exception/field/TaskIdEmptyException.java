package ru.t1.simakov.tm.exception.field;

public class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task id is empty...");
    }

}
