package ru.t1.simakov.tm.exception.field;

public class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException() {
        super("Error! This email exists, try another...");
    }

}
