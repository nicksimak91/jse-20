package ru.t1.simakov.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
