package ru.t1.simakov.tm.exception.field;

public class PasswordEmptyException extends  AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is incorrect...");
    }

}
