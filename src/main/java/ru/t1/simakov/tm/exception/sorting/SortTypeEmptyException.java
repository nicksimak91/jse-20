package ru.t1.simakov.tm.exception.sorting;

public class SortTypeEmptyException extends AbstractSortException {

    public SortTypeEmptyException() {
        super("Error! Empty sort value...");
    }

}
