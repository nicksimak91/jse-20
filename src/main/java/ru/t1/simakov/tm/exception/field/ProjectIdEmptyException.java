package ru.t1.simakov.tm.exception.field;

public class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project id is empty...");
    }

}
