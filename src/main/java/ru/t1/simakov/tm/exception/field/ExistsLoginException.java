package ru.t1.simakov.tm.exception.field;

public class ExistsLoginException extends AbstractFieldException {

    public ExistsLoginException() {
        super("Error! This login exists, try another...");
    }

}
