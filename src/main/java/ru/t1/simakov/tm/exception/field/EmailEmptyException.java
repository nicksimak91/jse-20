package ru.t1.simakov.tm.exception.field;

public class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! Email is incorrect...");
    }

}
